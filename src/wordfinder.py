from os import path

from urllib import request

import collections

ALPHABETS = {'i', 'u', 'y', 'm', 'j', 'd', 'w', 'g', 'e', 'b',
             'a', 'l', 's', 'f', 'c', 'o', 'k', 'x', 'n', 'z',
             'p', 'q', 't', 'h', 'v', 'r'}


class WordsFileDownloader:

    def __init__(self):
        self.__cached_words_file = path.join('.download', 'words.txt')

    def download(self):
        words_file_url = "http://www.mieliestronk.com/corncob_lowercase.txt"
        # words_file_url = "https://raw.githubusercontent.com/dwyl/english-words/master/words.txt"
        # words_file_url = "https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english-no-swears.txt"
        # words_file_url = "http://www.mit.edu/~ecprice/wordlist.10000"

        with request.urlopen(words_file_url) as site:
            content_length = site.info().get('Content-Length')
            content_length = content_length if content_length else 0
            remote_file_size = content_length

        assert (remote_file_size >= 0)

        valid_cache = False
        if path.isfile(self.__cached_words_file):
            cache_file_size = path.getsize(self.__cached_words_file)
            print("Remote file size : {remote_file_size}. Cache file size: {cache_file_size}"
                  .format(remote_file_size=remote_file_size, cache_file_size=cache_file_size))
            if cache_file_size == remote_file_size:
                valid_cache = True

        if not valid_cache:
            with open(self.__cached_words_file, mode='w') as file:
                with request.urlopen(words_file_url) as site:
                    data = site.read()
                    file.write(data.decode('utf-8'))
        return self.__cached_words_file


class CountNode:
    def __init__(self, count):
        self._count = count
        self._char_nodes = {}
        self._words = set()

    def get_char_node(self, char):
        return self._char_nodes.get(char, None)

    def add_word(self, word):
        self._words.add(word)

    def add_char_node(self, char):
        self._char_nodes[char] = CharNode(char)

    @property
    def words(self):
        return self._words


class CharNode:
    def __init__(self, char, count=None):
        self._char = char
        self._count_nodes = {count: CountNode(count)} if count else {}

    def get_count_node(self, count):
        return self._count_nodes.get(count, None)

    def add_count_node(self, count):
        self._count_nodes[count] = CountNode(count)

    @property
    def count_nodes(self):
        return self._count_nodes


class WordsTree:
    class WordId:
        def __init__(self, char, count):
            """

            :param char:
            :type char: ``str``
            :param count:
            :type count: ``int``
            """
            self.char = char
            self.count = count

    def __init__(self):
        self._nodes = {}

    @staticmethod
    def __get_word_id(word):
        """

        :param word:
        :type word:
        :return:
        :rtype: ``list`` of `WordsTree.WordId`
        """
        counter = collections.Counter(word)

        keys = list(counter.keys())
        keys.sort()

        ids = []
        for key in keys:
            ids.append(WordsTree.WordId(key, counter[key]))

        assert ids

        return ids

    def add_word(self, word):
        ids = self.__get_word_id(word)
        first_id = ids[0]
        if first_id.char not in self._nodes:
            self._nodes[first_id.char] = CharNode(first_id.char, first_id.count)
        self.__add_word_helper_count_node(ids, word, self._nodes[first_id.char])

    def __add_word_helper_char_node(self, ids, word, count_node):
        first_id = ids[0]
        if count_node.get_char_node(first_id.char) is None:
            count_node.add_char_node(first_id.char)

        char_node = count_node.get_char_node(first_id.char)
        self.__add_word_helper_count_node(ids, word, char_node)

    def __add_word_helper_count_node(self, ids, word, char_node):
        """

        :param ids:
        :type ids:
        :param word:
        :type word:
        :param char_node:
        :type char_node: ``CharNode``
        :return:
        :rtype:
        """
        count = ids[0].count

        if char_node.get_count_node(count) is None:
            char_node.add_count_node(count)

        count_node = char_node.get_count_node(count)
        if len(ids) == 1:
            count_node.add_word(word)
            # print("Word '{word}' added.".format(word=word))
            return

        ids.pop(0)
        self.__add_word_helper_char_node(ids, word, count_node)

    def find_words(self, chars, min_length, max_length):
        ids = self.__get_word_id(chars)
        found = list()

        i = 0
        while i < len(ids):
            if ids[i].char in self._nodes:
                self.__find_words_helper_char_node(ids, i, self._nodes[ids[i].char], found, min_length, max_length)
            i += 1
        return found

    def __find_words_helper_char_node(self, ids, i, char_node, found, min_length, max_length):

        count_nodes = char_node.count_nodes
        counts = list(char_node.count_nodes.keys())
        counts.sort()

        for count in counts:
            if count > ids[i].count:
                break
            self.__find_words_helper_count_node(ids, i, count_nodes[count], found, min_length, max_length)

    def __find_words_helper_count_node(self, ids, i, count_node, found, min_length, max_length):
        if count_node.words and min_length <= len(next(iter(count_node.words))) <= max_length:
            found.extend(count_node.words)

        j = i + 1
        while j < len(ids):
            if count_node.get_char_node(ids[j].char):
                self.__find_words_helper_char_node(ids, j, count_node.get_char_node(ids[j].char),
                                                   found, min_length, max_length)
            j += 1


class WordsFileReader:
    def __init__(self, words_file, include_non_alphabets=False):
        """

        :param words_file:
        :type words_file: ``str``
        """
        self.__words_file = words_file
        self.__include_non_alphabets = include_non_alphabets

    @staticmethod
    def __contains_only_alphabet(word):
        for char in word:
            if char not in ALPHABETS:
                return False
        return True

    def read(self):
        """

        :return:
        :rtype: ``WordsTree``
        """

        tree = WordsTree()
        with open(self.__words_file, mode='r') as file:
            text = file.readlines()
            for word in text:
                word = word.strip().lower()
                if not self.__contains_only_alphabet(word):
                    continue

                tree.add_word(word)

        return tree


file_path = WordsFileDownloader().download()
words_tree = WordsFileReader(file_path).read()


def print_words(words):
    for word in words:
        print(word)

while(True):
    chars = input("Chars: ")
    min_length = int(input("min_length: "))
    max_length = int(input("max length: "))
    print_words(words_tree.find_words(chars, min_length, max_length))
    print("-----------------------------------------------------")